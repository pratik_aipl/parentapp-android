package com.childrens.academy.listner;

public class PageReloadEvent {
    boolean isReload;
    String subjectIds, levelIds;

    public PageReloadEvent(boolean isReload, String subjectIds, String levelIds) {
        this.isReload = isReload;
        this.subjectIds = subjectIds;
        this.levelIds = levelIds;
    }

    public String getSubjectIds() {
        return subjectIds;
    }

    public String getLevelIds() {
        return levelIds;
    }

    public boolean isReload() {
        return isReload;
    }
}
