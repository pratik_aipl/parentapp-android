package com.childrens.academy.listner;


import com.childrens.academy.bean.PaperBean;

public interface TestOnClick {
    public void callbackPeep(int pos, PaperBean paperBean);
}
