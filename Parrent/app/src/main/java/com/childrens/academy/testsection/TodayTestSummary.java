package com.childrens.academy.testsection;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bluelinelabs.logansquare.LoganSquare;
import com.childrens.academy.R;
import com.childrens.academy.adapter.SummaryViewPagerAdapter;
import com.childrens.academy.base.ActivityBase;
import com.childrens.academy.bean.TestDetails;
import com.childrens.academy.bean.TestSummary;
import com.childrens.academy.network.NetworkRequest;
import com.childrens.academy.utils.Constant;
import com.childrens.academy.utils.Utils;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscription;

public class TodayTestSummary extends ActivityBase {

    @BindView(R.id.mBackBtn)
    ImageView mBackBtn;
    @BindView(R.id.mPageTitle)
    TextView mPageTitle;
    @BindView(R.id.mSearch)
    ImageView mSearch;
    @BindView(R.id.progressBarCorrect)
    ProgressBar progressBarCorrect;
    @BindView(R.id.progressBarInCorrect)
    ProgressBar progressBarInCorrect;
    @BindView(R.id.progressBarNotAppeared)
    ProgressBar progressBarNotAppeared;
    @BindView(R.id.tabs)
    TabLayout tablayout;
    @BindView(R.id.viewpager)
    ViewPager viewpager;
    @BindView(R.id.mRightText)
    TextView mRightText;
    @BindView(R.id.mEndTest)
    Button mEndTest;
    @BindView(R.id.mCorrect)
    TextView mCorrect;
    @BindView(R.id.mIncorrect)
    TextView mIncorrect;
    @BindView(R.id.mNotAppeared)
    TextView mNotAppeared;

    SummaryViewPagerAdapter adapter;
    Subscription subscriptionTestSummary;
    TestSummary testSummary;
    String studentMCQTestID;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.today_test_summary);
        ButterKnife.bind(this);

        studentMCQTestID = getIntent().getStringExtra(Constant.StudentMCQTestID);
        if (Utils.isNetworkAvailable(this, true))
            testSummary(true);

        tablayout.setupWithViewPager(viewpager);
        mPageTitle.setText("Test Summary");
    }


    private void createViewPager(ViewPager viewPager) {
        adapter = new SummaryViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(Correct.newInstance(testSummary.getCorrectQuestions()), Constant.correct);
        adapter.addFrag(InCorrect.newInstance(testSummary.getIncorrectQuestions()), Constant.incorrect);
        adapter.addFrag(NotAppeared.newInstance(testSummary.getNotAppearedQuestions()), Constant.notappeared);
        viewPager.setAdapter(adapter);

        viewPager.setOffscreenPageLimit(3);
    }


    @OnClick(R.id.mBackBtn)
    public void onViewClicked() {
        onBackPressed();
    }


    private void testSummary(boolean isShow) {
        Map<String, String> map = new HashMap<>();
        map.put(Constant.PaperID, studentMCQTestID);
        showProgress(isShow);
        subscriptionTestSummary = NetworkRequest.performAsyncRequest(restApi.testSummary(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());
                    testSummary = LoganSquare.parse(jsonResponse.getJSONObject(Constant.data).toString(), TestSummary.class);
                    createViewPager(viewpager);
                    setData();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                Utils.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });
    }

    private void setData() {
        TestDetails testDetails = testSummary.getTestDetails();
        mCorrect.setText("" + testDetails.getTotalRight() + " - " + testDetails.getTotalQuestion());
        mIncorrect.setText("" + testDetails.getTotalWrong() + " - " + testDetails.getTotalQuestion());
        mNotAppeared.setText("" + (testDetails.getTotalQuestion() - testDetails.getTotalAttempt()) + " - " + testDetails.getTotalQuestion());

        progressBarCorrect.setMax(testDetails.getTotalQuestion());
        progressBarInCorrect.setMax(testDetails.getTotalQuestion());
        progressBarNotAppeared.setMax(testDetails.getTotalQuestion());

        progressBarCorrect.setProgress(testDetails.getTotalRight());
        progressBarInCorrect.setProgress(testDetails.getTotalWrong());
        progressBarNotAppeared.setProgress((testDetails.getTotalQuestion() - testDetails.getTotalAttempt()));

    }
}
