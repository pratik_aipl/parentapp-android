package com.childrens.academy.bean;

public class AnswerDetailBean {

    int MCQPlannerDetailsID;
    int AnswerID;
    String IsAttempt;

    public AnswerDetailBean() {
    }

    public AnswerDetailBean(int MCQPlannerDetailsID, int answerID, String isAttempt) {
        this.MCQPlannerDetailsID = MCQPlannerDetailsID;
        AnswerID = answerID;
        IsAttempt = isAttempt;
    }

    public int getMCQPlannerDetailsID() {
        return MCQPlannerDetailsID;
    }

    public int getAnswerID() {
        return AnswerID;
    }

    public String getIsAttempt() {
        return IsAttempt;
    }
}
