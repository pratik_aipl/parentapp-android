package com.childrens.academy.bean;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;
import java.util.List;

@JsonObject
public class QuestionsBean implements Serializable {

    @JsonField(name = "MCQPlannerDetailsID")
    int MCQPlannerDetailsID;
    @JsonField(name = "MCQPlannerID")
    int MCQPlannerID;
    @JsonField(name = "MCQQuestionID")
    int MCQQuestionID;
    @JsonField(name = "ChapterID")
    int ChapterID;
    @JsonField(name = "Question")
    String Question;
    @JsonField(name = "LevelID")
    int LevelID;
    @JsonField(name = "Duration")
    long Duration;
    @JsonField(name = "Options")
    List<OptionBean> Options;

    public int getMCQPlannerDetailsID() {
        return MCQPlannerDetailsID;
    }

    public void setMCQPlannerDetailsID(int MCQPlannerDetailsID) {
        this.MCQPlannerDetailsID = MCQPlannerDetailsID;
    }

    public int getMCQPlannerID() {
        return MCQPlannerID;
    }

    public void setMCQPlannerID(int MCQPlannerID) {
        this.MCQPlannerID = MCQPlannerID;
    }

    public int getMCQQuestionID() {
        return MCQQuestionID;
    }

    public void setMCQQuestionID(int MCQQuestionID) {
        this.MCQQuestionID = MCQQuestionID;
    }

    public int getChapterID() {
        return ChapterID;
    }

    public void setChapterID(int chapterID) {
        ChapterID = chapterID;
    }

    public String getQuestion() {
        return Question;
    }

    public void setQuestion(String question) {
        Question = question;
    }

    public int getLevelID() {
        return LevelID;
    }

    public void setLevelID(int levelID) {
        LevelID = levelID;
    }

    public long getDuration() {
        return Duration;
    }

    public void setDuration(long duration) {
        Duration = duration;
    }

    public List<OptionBean> getOptions() {
        return Options;
    }

    public void setOptions(List<OptionBean> options) {
        Options = options;
    }
}
