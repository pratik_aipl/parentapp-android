package com.childrens.academy.bean;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

@JsonObject
public class Subjectaccuracy {
    @JsonField(name = "subjectaccuracy")
    int subjectAccuracy;

    public int getSubjectAccuracy() {
        return subjectAccuracy;
    }

    public void setSubjectAccuracy(int subjectAccuracy) {
        this.subjectAccuracy = subjectAccuracy;
    }
}
