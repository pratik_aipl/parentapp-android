package com.childrens.academy.base;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.childrens.academy.bean.UserData;
import com.childrens.academy.network.RestAPIBuilder;
import com.childrens.academy.network.RestApi;
import com.childrens.academy.utils.Constant;
import com.childrens.academy.utils.Prefs;
import com.google.gson.Gson;
import com.tbruyelle.rxpermissions2.RxPermissions;


public class ActivityBase extends AppCompatActivity {

    private static final int NUM_OF_ITEMS = 100;

    protected RxPermissions rxPermissions;
    public Prefs prefs;
    ProgressDialog progressDialog;
    protected Gson gson;
    protected RestApi restApi;
    public UserData user;
    public String registrationId="";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        rxPermissions = new RxPermissions(this);
        restApi = RestAPIBuilder.buildRetrofitService();
        prefs = Prefs.with(this);
        gson = new Gson();
        user = gson.fromJson(prefs.getString(Constant.UserData, ""), UserData.class);

    }

    public void showProgress(boolean isShow) {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(this);
            progressDialog.setMessage("Loading...");
        }
        if (isShow && !progressDialog.isShowing()) {
            progressDialog.show();
        } else {
            progressDialog.dismiss();
        }
    }
}
