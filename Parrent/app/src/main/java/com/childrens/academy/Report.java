package com.childrens.academy;

import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bluelinelabs.logansquare.LoganSquare;
import com.childrens.academy.adapter.TestDetailsSubjectAdapter;
import com.childrens.academy.base.ActivityBase;
import com.childrens.academy.base.NetworkStateReceiver;
import com.childrens.academy.bean.SubjectBean;
import com.childrens.academy.bean.TestSummaryDetails;
import com.childrens.academy.listner.DialogButtonListener;
import com.childrens.academy.listner.TestDetailsSubjectOnClick;
import com.childrens.academy.network.NetworkRequest;
import com.childrens.academy.utils.Constant;
import com.childrens.academy.utils.Utils;
import com.childrens.academy.view.NonScrollRecyclerView;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscription;

public class Report extends ActivityBase implements TestDetailsSubjectOnClick, NetworkStateReceiver.NetworkStateReceiverListener {


    @BindView(R.id.mBackBtn)
    ImageView mBackBtn;
    @BindView(R.id.mPageTitle)
    TextView mPageTitle;
    @BindView(R.id.mSearch)
    ImageView mSearch;
    @BindView(R.id.mRightText)
    TextView mRightText;
    @BindView(R.id.mEndTest)
    Button mEndTest;
    @BindView(R.id.mHeadNot)
    ImageView mHeadNot;
    @BindView(R.id.mPerformance)
    TextView mPerformance;
    @BindView(R.id.mCompleted)
    TextView mCompleted;
    @BindView(R.id.mMissed)
    TextView mMissed;
    @BindView(R.id.mCount)
    TextView mCount;

    TestDetailsSubjectAdapter testDetailsSubjectAdapter;
    @BindView(R.id.mAssignmentList)
    NonScrollRecyclerView mAssignmentList;
    @BindView(R.id.mEmptyView)
    RelativeLayout mEmptyView;
    @BindView(R.id.mHintView)
    LinearLayout mHintView;

    private static final String TAG = "Report";
    public NetworkStateReceiver networkStateReceiver;
    Subscription subscriptionSubjectList;
    TestSummaryDetails testSummaryDetails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);
        ButterKnife.bind(this);

        networkStateReceiver = new NetworkStateReceiver();
        networkStateReceiver.addListener(this);
        this.registerReceiver(networkStateReceiver, new IntentFilter(android.net.ConnectivityManager.CONNECTIVITY_ACTION));


        mPageTitle.setText("Reports");


    }

    @OnClick({R.id.mBackBtn, R.id.mSearch})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.mBackBtn:
                onBackPressed();
                break;
            case R.id.mSearch:
                break;
        }
    }

    @Override
    public void callbackPaper(int pos, SubjectBean subjectBean) {
        if (Utils.isNetworkAvailable(this, true))
            startActivity(new Intent(this, SubjectReport.class).putExtra(Constant.subjectReport, subjectBean));
    }

    private void summaryDetails(boolean isShow) {
        Map<String, String> map = new HashMap<>();

        showProgress(isShow);
        subscriptionSubjectList = NetworkRequest.performAsyncRequest(restApi.getTestSummaryDetails(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    Log.d(TAG, "summaryDetails: " + data.body());
                    JSONObject jsonResponse = new JSONObject(data.body());
                    testSummaryDetails = LoganSquare.parse(jsonResponse.getJSONObject(Constant.data).toString(), TestSummaryDetails.class);
                    setData();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                Utils.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });
    }

    private void setData() {

        mPerformance.setText(testSummaryDetails.getOverallPerformance() + "%");
        mCount.setText(getString(R.string.total_20, (testSummaryDetails.getTotalAttempt()) + testSummaryDetails.getTotalMissed()));
        mCompleted.setText(testSummaryDetails.getTotalAttempt() + "");
        mMissed.setText(testSummaryDetails.getTotalMissed() + "");

        testDetailsSubjectAdapter = new TestDetailsSubjectAdapter(this, testSummaryDetails.getSubject());
        mAssignmentList.setAdapter(testDetailsSubjectAdapter);
//        mHintView.setVisibility(View.VISIBLE);
        if (testSummaryDetails.getSubject().size() > 0) {
            mAssignmentList.setVisibility(View.VISIBLE);
            mEmptyView.setVisibility(View.GONE);
        } else {
            mAssignmentList.setVisibility(View.GONE);
            mEmptyView.setVisibility(View.VISIBLE);
        }
    }


    @Override
    protected void onDestroy() {
        if (subscriptionSubjectList != null && !subscriptionSubjectList.isUnsubscribed()) {
            subscriptionSubjectList.unsubscribe();
            subscriptionSubjectList = null;
        }
        if (EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().unregister(this);
        super.onDestroy();
        networkStateReceiver.removeListener(this);
        this.unregisterReceiver(networkStateReceiver);
    }

    @Override
    public void networkAvailable() {
        summaryDetails(true);
    }

    @Override
    public void networkUnavailable() {
        if (!Constant.isAlertShow) {
            Constant.isAlertShow = true;
            Utils.showTwoButtonDialog(this, getString(R.string.network_error_title), getString(R.string.network_connection_error), "Ok", null, new DialogButtonListener() {
                @Override
                public void onPositiveButtonClicked() {
                    Constant.isAlertShow = false;
                }

                @Override
                public void onNegativButtonClicked() {
                }
            });
        }
    }
}
