package com.childrens.academy;

import android.Manifest;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.childrens.academy.base.ActivityBase;
import com.childrens.academy.utils.Constant;

public class SplashScreen extends ActivityBase {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        int SPLASH_TIME_OUT = 1500;
        new Handler().postDelayed(() -> {
            if (prefs.getBoolean(Constant.isLogin, false)) {
                Intent i = new Intent(this, DashBoard.class);
                startActivity(i);
            } else {
                Intent i = new Intent(this, MobileScreen.class);
                startActivity(i);
            }
            finish();
        }, SPLASH_TIME_OUT);


    }
}
