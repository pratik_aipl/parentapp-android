package com.childrens.academy.bean;


import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;

@JsonObject
public class LevelBean implements Serializable {


    @JsonField
    String LevelID;

    @JsonField
    String LevelName;

    @JsonField
    String Level;

    @JsonField
    int Count;

    public String getLevel() {
        return Level;
    }

    public void setLevel(String level) {
        Level = level;
    }

    public boolean isSelectLevel() {
        return isSelectLevel;
    }

    public void setSelectLevel(boolean selectLevel) {
        isSelectLevel = selectLevel;
    }

    boolean isSelectLevel = false;

    public String getLevelID() {
        return LevelID;
    }

    public void setLevelID(String levelID) {
        LevelID = levelID;
    }

    public String getLevelName() {
        return LevelName;
    }

    public void setLevelName(String levelName) {
        LevelName = levelName;
    }

    public int getCount() {
        return Count;
    }

    public void setCount(int count) {
        Count = count;
    }
}
