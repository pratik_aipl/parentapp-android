package com.childrens.academy.testsection;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.childrens.academy.R;
import com.childrens.academy.adapter.LevelAdapter;
import com.childrens.academy.adapter.OptionAdapter;
import com.childrens.academy.base.BaseFragment;
import com.childrens.academy.bean.OptionBean;
import com.childrens.academy.bean.QuestionsBean;
import com.childrens.academy.utils.Constant;
import com.childrens.academy.view.QuestionsWebView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class QuestionPageFragment extends BaseFragment {

    @BindView(R.id.mQuestionText)
    QuestionsWebView mQuestionText;
    @BindView(R.id.mOptionList)
    RecyclerView mOptionList;
    @BindView(R.id.mViewContainer)
    LinearLayout mViewContainer;
    Unbinder unbinder;

    OptionAdapter optionAdapter;
    QuestionsBean questionsBean;
    List<OptionBean> optionBeans = new ArrayList<>();

    // Store instance variables

    public static QuestionPageFragment newInstance(int i, QuestionsBean questionsBean) {
        QuestionPageFragment fragmentFirst = new QuestionPageFragment();
        Bundle args = new Bundle();
        args.putSerializable(Constant.selQuestion, questionsBean);
        fragmentFirst.setArguments(args);
        return fragmentFirst;
    }

    // Store instance variables based on arguments passed
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        questionsBean = (QuestionsBean) getArguments().getSerializable(Constant.selQuestion);
    }

    // Inflate the view for the fragment based on layout XML
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_question_page, container, false);
        unbinder = ButterKnife.bind(this, view);
        mQuestionText.loadHtmlFromLocal(questionsBean.getQuestion());

        mQuestionText.setBackgroundColor(Color.TRANSPARENT);
        mQuestionText.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        mQuestionText.setScrollbarFadingEnabled(false);
        mQuestionText.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null);
        /*
         WebSettings settings = mQuestionText.getSettings();


        settings.setAllowFileAccess(true);
        settings.setBuiltInZoomControls(true);
        settings.setDomStorageEnabled(true);
        settings.setUseWideViewPort(true);
        settings.setDefaultZoom(WebSettings.ZoomDensity.CLOSE);
        settings.setLoadWithOverviewMode(true);
        mQuestionText.setInitialScale(1);
        settings.setAllowFileAccess(true);
        settings.setAllowContentAccess(true);
        settings.setAllowFileAccessFromFileURLs(true);
        settings.setLoadWithOverviewMode(true);
        settings.setUseWideViewPort(true);
        settings.setBuiltInZoomControls(true);
        settings.setJavaScriptEnabled(true);
        settings.setSupportZoom(true);
        settings.setDomStorageEnabled(true);
         */
        optionBeans.clear();
        optionBeans.addAll(questionsBean.getOptions());
        mOptionList.setLayoutManager(new LinearLayoutManager(getActivity()));
        optionAdapter = new OptionAdapter(getActivity(), optionBeans);
        mOptionList.setAdapter(optionAdapter);
        return view;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
