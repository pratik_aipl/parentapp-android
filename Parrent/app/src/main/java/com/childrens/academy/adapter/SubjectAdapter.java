package com.childrens.academy.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.childrens.academy.R;
import com.childrens.academy.bean.SubjectBean;
import com.childrens.academy.listner.SubjectOnClick;
import com.childrens.academy.searchsection.BottomSheet3DialogFragment;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Peep on 04/12/18.
 */
public class SubjectAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public final static int VIEW_TYPE_ITEM = 0;
    Context context;
    List<SubjectBean> subjectBeans;
    SubjectOnClick subjectOnClick;

    public SubjectAdapter(Context context, List<SubjectBean> subjectBeans, BottomSheet3DialogFragment onclick) {
        this.context = context;
        this.subjectBeans = subjectBeans;
        this.subjectOnClick = (SubjectOnClick) onclick;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.raw_filter_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holderIn, int position) {
        ViewHolder holder = (ViewHolder) holderIn;
        SubjectBean subjectBean=subjectBeans.get(position);
        holder.mTitleName.setText(subjectBean.getSubjectName());
        if (position == 0)
            holder.mViewContainer.setBackground(ContextCompat.getDrawable(context, R.drawable.list_top_cornor_round));
        else if (position == subjectBeans.size()-1)
            holder.mViewContainer.setBackground(ContextCompat.getDrawable(context, R.drawable.list_bottom_cornor_round));
        else
            holder.mViewContainer.setBackground(ContextCompat.getDrawable(context, R.drawable.list_no_round));

        holder.mTitleName.setOnClickListener(v -> {
            if (holder.mRadioBtn.isChecked())
                holder.mRadioBtn.setChecked(false);
            else
                holder.mRadioBtn.setChecked(true);
            subjectBean.setSelectSubject(holder.mRadioBtn.isChecked());
            subjectOnClick.callBackSubject(position, subjectBean);
        });

        holder.mImageView.setOnClickListener(v -> {
            if (holder.mRadioBtn.isChecked())
                holder.mRadioBtn.setChecked(false);
            else
                holder.mRadioBtn.setChecked(true);
            subjectBean.setSelectSubject(holder.mRadioBtn.isChecked());
            subjectOnClick.callBackSubject(position, subjectBean);
        });
    }

    @Override
    public int getItemViewType(int position) {
        return VIEW_TYPE_ITEM;
    }

    @Override
    public int getItemCount() {

        return subjectBeans.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.mViewContainer)
        public LinearLayout mViewContainer;
        @BindView(R.id.mTitleName)
        public TextView mTitleName;
        @BindView(R.id.mRadioBtn)
        public RadioButton mRadioBtn;
        @BindView(R.id.mImageView)
        ImageView mImageView;
        public ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }

}
