package com.childrens.academy.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.childrens.academy.R;
import com.childrens.academy.bean.PaperBean;
import com.childrens.academy.listner.TestOnClick;
import com.childrens.academy.testsection.CCT;
import com.childrens.academy.utils.Utils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Peep on 04/12/18.
 */
public class CCTAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public final static int VIEW_TYPE_ITEM = 0;
    Context context;
    List<PaperBean> paperBeans;
    TestOnClick testOnClick;

    public CCTAdapter(CCT cct, List<PaperBean> paperBeans) {
        this.context = cct;
        this.paperBeans = paperBeans;
        this.testOnClick = cct;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.raw_cct_item, parent, false));


    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holderIn, int position) {
        ViewHolder holder = (ViewHolder) holderIn;
        PaperBean paperBean = paperBeans.get(position);
        if (position == 0)
            holder.mViewContainer.setBackground(ContextCompat.getDrawable(context, R.drawable.list_top_cornor_round));
        else if (position == paperBeans.size() - 1)
            holder.mViewContainer.setBackground(ContextCompat.getDrawable(context, R.drawable.list_bottom_cornor_round));
        else
            holder.mViewContainer.setBackground(ContextCompat.getDrawable(context, R.drawable.list_no_round));

        holder.mViewContainer.setOnClickListener(v -> testOnClick.callbackPeep(position, paperBean));

        holder.mDay.setText(Utils.changeDateToDay(paperBean.getStartDate()));
        holder.mMonth.setText(Utils.changeDateToMonth(paperBean.getStartDate()));
        holder.mPaperName.setText(paperBean.getSubjectName());
        holder.mExpireDate.setText("Expire on : "+Utils.changeDateToDDMMMYYYY(paperBean.getEndDate()));



    }

    @Override
    public int getItemViewType(int position) {
        return VIEW_TYPE_ITEM;
    }

    @Override
    public int getItemCount() {

        return paperBeans.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.mDay)
        TextView mDay;
        @BindView(R.id.mMonth)
        TextView mMonth;
        @BindView(R.id.mPaperName)
        TextView mPaperName;
        @BindView(R.id.mExpireDate)
        TextView mExpireDate;
        @BindView(R.id.mNextArrow)
        ImageView mNextArrow;
        @BindView(R.id.mViewContainer)
        LinearLayout mViewContainer;

        public ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }

}
