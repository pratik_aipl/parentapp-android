package com.childrens.academy.bean;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.util.List;

@JsonObject
public class TestDetails {

    @JsonField(name = "StudentMCQTestID")
    int StudentMCQTestID;
    @JsonField(name = "StudentID")
    int StudentID;
    @JsonField(name = "PlannerID")
    int PlannerID;
    @JsonField(name = "TotalQuestion")
    int TotalQuestion;
    @JsonField(name = "TotalRight")
    int TotalRight;
    @JsonField(name = "TotalWrong")
    int TotalWrong;
    @JsonField(name = "TotalAttempt")
    int TotalAttempt;
    @JsonField(name = "Accuracy")
    int Accuracy;
    @JsonField(name = "SubmitDate")
    String SubmitDate;
    @JsonField(name = "TakenTime")
    long TakenTime;
    @JsonField(name = "AvgTime")
    long AvgTime;
    @JsonField(name = "SubjectID")
    int SubjectID;
    @JsonField(name = "SubjectName")
    String SubjectName;
    @JsonField(name = "subjectaccuracy")
    List<Subjectaccuracy> subjectAccuracy;
//    String subjectAccuracy;

    public List<Subjectaccuracy> getSubjectAccuracy() {
        return subjectAccuracy;
    }

    public void setSubjectAccuracy(List<Subjectaccuracy> subjectAccuracy) {
        this.subjectAccuracy = subjectAccuracy;
    }

    public long getTakenTime() {
        return TakenTime;
    }

    public void setTakenTime(long takenTime) {
        TakenTime = takenTime;
    }

    public long getAvgTime() {
        return AvgTime;
    }

    public void setAvgTime(long avgTime) {
        AvgTime = avgTime;
    }



    public int getStudentMCQTestID() {
        return StudentMCQTestID;
    }

    public void setStudentMCQTestID(int studentMCQTestID) {
        StudentMCQTestID = studentMCQTestID;
    }

    public int getStudentID() {
        return StudentID;
    }

    public void setStudentID(int studentID) {
        StudentID = studentID;
    }

    public int getPlannerID() {
        return PlannerID;
    }

    public void setPlannerID(int plannerID) {
        PlannerID = plannerID;
    }

    public int getTotalQuestion() {
        return TotalQuestion;
    }

    public void setTotalQuestion(int totalQuestion) {
        TotalQuestion = totalQuestion;
    }

    public int getTotalRight() {
        return TotalRight;
    }

    public void setTotalRight(int totalRight) {
        TotalRight = totalRight;
    }

    public int getTotalWrong() {
        return TotalWrong;
    }

    public void setTotalWrong(int totalWrong) {
        TotalWrong = totalWrong;
    }

    public int getTotalAttempt() {
        return TotalAttempt;
    }

    public void setTotalAttempt(int totalAttempt) {
        TotalAttempt = totalAttempt;
    }

    public int getAccuracy() {
        return Accuracy;
    }

    public void setAccuracy(int accuracy) {
        Accuracy = accuracy;
    }

    public String getSubmitDate() {
        return SubmitDate;
    }

    public void setSubmitDate(String submitDate) {
        SubmitDate = submitDate;
    }

    public int getSubjectID() {
        return SubjectID;
    }

    public void setSubjectID(int subjectID) {
        SubjectID = subjectID;
    }

    public String getSubjectName() {
        return SubjectName;
    }

    public void setSubjectName(String subjectName) {
        SubjectName = subjectName;
    }

}
