package com.childrens.academy.searchsection;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bluelinelabs.logansquare.LoganSquare;
import com.childrens.academy.R;
import com.childrens.academy.adapter.SearchTestAdapter;
import com.childrens.academy.base.ActivityBase;
import com.childrens.academy.bean.LevelBean;
import com.childrens.academy.bean.SearchBean;
import com.childrens.academy.bean.SubjectBean;
import com.childrens.academy.listner.PageReloadEvent;
import com.childrens.academy.listner.SearchOnClick;
import com.childrens.academy.network.NetworkRequest;
import com.childrens.academy.testsection.TodayTestReport;
import com.childrens.academy.utils.Constant;
import com.childrens.academy.utils.Utils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscription;

public class SearchTestPaper extends ActivityBase implements SearchOnClick {

    private static final String TAG = "SearchTestPaper";
    @BindView(R.id.mBackBtn)
    ImageView mBackBtn;
    @BindView(R.id.mPageTitle)
    TextView mPageTitle;
    @BindView(R.id.mSearch)
    ImageView mSearch;

    SearchTestAdapter searchTestAdapter;
    @BindView(R.id.mRecyclerView)
    RecyclerView mRecyclerView;
    List<SearchBean> searchBeans = new ArrayList<>();
    List<LevelBean> levelBeanList = new ArrayList<>();
    List<SubjectBean> subjectBeanList = new ArrayList<>();

    Subscription subscriptionSearchPaperList, subscriptionSubjectList, subscriptionLevelList;
    public BottomSheet3DialogFragment bottomSheet3DialogFragment;
    @BindView(R.id.mEmpty)
    ImageView mEmpty;
    @BindView(R.id.tv_empty)
    TextView tvEmpty;
    @BindView(R.id.mEmptyView)
    RelativeLayout mEmptyView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_paper);
        ButterKnife.bind(this);
        mPageTitle.setText("Search ASSIGNMENTS");
        mSearch.setVisibility(View.VISIBLE);
        mSearch.setImageResource(R.mipmap.filter);

        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }

        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        searchTestAdapter = new SearchTestAdapter(this, searchBeans);
        mRecyclerView.setAdapter(searchTestAdapter);

        if (Utils.isNetworkAvailable(this, true)) {
            getSearchPapers(true, "", "");
            getSubjectList(false);
            getLevelList(false);

        }

    }

    private void getSearchPapers(boolean isShow, String subjectIds, String levelIds) {
        Map<String, String> map = new HashMap<>();
        if (!TextUtils.isEmpty(subjectIds))
            map.put(Constant.SubjectID, subjectIds);
        if (!TextUtils.isEmpty(levelIds))
            map.put(Constant.LevelID, levelIds);

        Log.d(TAG, "getSearchPapers: " + map);
        showProgress(isShow);
        subscriptionSearchPaperList = NetworkRequest.performAsyncRequest(restApi.getSearchPaperList(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());
                    searchBeans.clear();
                    searchBeans.addAll(LoganSquare.parseList(jsonResponse.getJSONArray(Constant.data).toString(), SearchBean.class));
                    searchTestAdapter.notifyDataSetChanged();
                    if (searchBeans.size()>0){
                        mRecyclerView.setVisibility(View.VISIBLE);
                        mEmptyView.setVisibility(View.GONE);
                    }else {
                        mRecyclerView.setVisibility(View.GONE);
                        mEmptyView.setVisibility(View.VISIBLE);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                Utils.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });
    }

    private void getSubjectList(boolean isShow) {
        Map<String, String> map = new HashMap<>();
        subscriptionSubjectList = NetworkRequest.performAsyncRequest(restApi.getSubjectList(map), (data) -> {
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());
                    subjectBeanList.clear();
                    subjectBeanList.addAll(LoganSquare.parseList(jsonResponse.getJSONArray(Constant.data).toString(), SubjectBean.class));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                Utils.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            e.printStackTrace();
        });
    }

    private void getLevelList(boolean isShow) {
        Map<String, String> map = new HashMap<>();
        subscriptionLevelList = NetworkRequest.performAsyncRequest(restApi.getLevelList(map), (data) -> {
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());
                    levelBeanList.clear();
                    levelBeanList.addAll(LoganSquare.parseList(jsonResponse.getJSONArray(Constant.data).toString(), LevelBean.class));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                Utils.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            e.printStackTrace();
        });
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onPageReloadEvent(PageReloadEvent event) {
        getSearchPapers(true, event.getSubjectIds(), event.getLevelIds());
    }


    @OnClick({R.id.mBackBtn, R.id.mSearch})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.mBackBtn:
                onBackPressed();
                break;
            case R.id.mSearch:
                bottomSheet3DialogFragment = BottomSheet3DialogFragment.newInstance(levelBeanList, subjectBeanList);
                bottomSheet3DialogFragment.show(getSupportFragmentManager(), "Custom Bottom Sheet");
                break;
        }
    }

    @Override
    public void callbackPeep(int pos, SearchBean searchBean) {
        startActivity(new Intent(this, TodayTestReport.class).putExtra(Constant.StudentMCQTestID, searchBean.getStudentMCQTestID()));
    }

    @Override
    protected void onDestroy() {
        if (subscriptionLevelList != null && !subscriptionLevelList.isUnsubscribed()) {
            subscriptionLevelList.unsubscribe();
            subscriptionLevelList = null;
        }
        if (subscriptionSearchPaperList != null && !subscriptionSearchPaperList.isUnsubscribed()) {
            subscriptionSearchPaperList.unsubscribe();
            subscriptionSearchPaperList = null;
        }
        if (subscriptionSubjectList != null && !subscriptionSubjectList.isUnsubscribed()) {
            subscriptionSubjectList.unsubscribe();
            subscriptionSubjectList = null;
        }
        if (EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().unregister(this);
        super.onDestroy();
    }
}
