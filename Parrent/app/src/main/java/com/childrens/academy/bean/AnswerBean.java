package com.childrens.academy.bean;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;
import java.util.List;

@JsonObject
public class AnswerBean implements Serializable {

    /*
     "MCQQuestionID": "1",
        "Question": "q<\/p>",
        "AnswerID": "2",
     */
    @JsonField(name = "MCQQuestionID")
    int MCQQuestionID;
    @JsonField(name = "Question")
    String Question;
    @JsonField(name = "AnswerID")
    int AnswerID;

    @JsonField(name = "Options")
    List<OptionBean> optionBeans;


    public int getMCQQuestionID() {
        return MCQQuestionID;
    }

    public void setMCQQuestionID(int MCQQuestionID) {
        this.MCQQuestionID = MCQQuestionID;
    }

    public String getQuestion() {
        return Question;
    }

    public void setQuestion(String question) {
        Question = question;
    }

    public int getAnswerID() {
        return AnswerID;
    }

    public void setAnswerID(int answerID) {
        AnswerID = answerID;
    }

    public List<OptionBean> getOptionBeans() {
        return optionBeans;
    }

    public void setOptionBeans(List<OptionBean> optionBeans) {
        this.optionBeans = optionBeans;
    }
}

