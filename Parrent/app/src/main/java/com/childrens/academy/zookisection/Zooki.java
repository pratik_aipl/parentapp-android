package com.childrens.academy.zookisection;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bluelinelabs.logansquare.LoganSquare;
import com.childrens.academy.R;
import com.childrens.academy.base.ActivityBase;
import com.childrens.academy.bean.ZookiBean;
import com.childrens.academy.network.NetworkRequest;
import com.childrens.academy.utils.Constant;
import com.childrens.academy.utils.CustPagerTransformer;
import com.childrens.academy.utils.Utils;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscription;

public class Zooki extends ActivityBase {

    @BindView(R.id.mBackBtn)
    ImageView mBackBtn;
    @BindView(R.id.mPageTitle)
    TextView mPageTitle;
    @BindView(R.id.mSearch)
    ImageView mSearch;
    @BindView(R.id.mRightText)
    TextView mRightText;
    @BindView(R.id.mPager)
    ViewPager viewPager;
    public ArrayList<ZookiBean> zookiList = new ArrayList<>();
    private List<ZookiFragment> fragments = new ArrayList<>();
    private int Postion = 0;

    Subscription subscriptionZooki;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zooki);
        ButterKnife.bind(this);

        mRightText.setVisibility(View.VISIBLE);
        mPageTitle.setText("Zooki");

        if (Utils.isNetworkAvailable(this, true)) {
            callZooki(true);
        }

    }


    private void callZooki(boolean isShow) {
        Map<String, String> map = new HashMap<>();
        showProgress(isShow);
        subscriptionZooki = NetworkRequest.performAsyncRequest(restApi.getZookiList(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());
                    prefs.save(Constant.folderPath, jsonResponse.getString(Constant.folderPath));
                    zookiList.clear();
                    zookiList.addAll(LoganSquare.parseList(jsonResponse.getJSONArray(Constant.data).toString(), ZookiBean.class));
                    fillViewPager();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
               Utils.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });
    }


    private void fillViewPager() {
        viewPager.setPageTransformer(false, new CustPagerTransformer(this));
        for (int i = 0; i < zookiList.size(); i++) {
            fragments.add(new ZookiFragment());
        }
        viewPager.setAdapter(new FragmentStatePagerAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                ZookiFragment fragment = fragments.get(position % 10);
                fragment.bindData(zookiList.get(position % zookiList.size()));
                return fragment;
            }

            @Override
            public boolean isViewFromObject(View view, Object object) {
                return super.isViewFromObject(view, object);
            }

            @Override
            public int getCount() {
                return zookiList.size();
            }
        });

        viewPager.setCurrentItem(Postion);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                updateIndicatorTv();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        updateIndicatorTv();
    }

    private void updateIndicatorTv() {
        int totalNum = viewPager.getAdapter().getCount();
        int currentItem = viewPager.getCurrentItem() + 1;
        mRightText.setText(currentItem +" / "+ totalNum);
    }

    @OnClick({R.id.mBackBtn, R.id.mSearch})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.mBackBtn:
                onBackPressed();
                break;
            case R.id.mSearch:
                break;
        }
    }
}
