package com.childrens.academy.listner;

public interface DialogButtonListener {
    void onPositiveButtonClicked();
    void onNegativButtonClicked();
}