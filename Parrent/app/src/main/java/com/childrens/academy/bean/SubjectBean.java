package com.childrens.academy.bean;


import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;

@JsonObject
public class SubjectBean implements Serializable {

    @JsonField
    String SubjectID;

    @JsonField
    String SubjectName;

    @JsonField
    int OverallPerformance;
    @JsonField
    int TotalMissed;
    @JsonField
    int TotalAssign;
    @JsonField
    int TotalAttempt;

    boolean isSelectSubject = false;

    public int getOverallPerformance() {
        return OverallPerformance;
    }

    public void setOverallPerformance(int overallPerformance) {
        OverallPerformance = overallPerformance;
    }

    public int getTotalMissed() {
        return TotalMissed;
    }

    public void setTotalMissed(int totalMissed) {
        TotalMissed = totalMissed;
    }

    public int getTotalAssign() {
        return TotalAssign;
    }

    public void setTotalAssign(int totalAssign) {
        TotalAssign = totalAssign;
    }

    public int getTotalAttempt() {
        return TotalAttempt;
    }

    public void setTotalAttempt(int totalAttempt) {
        TotalAttempt = totalAttempt;
    }


    public boolean isSelectSubject() {
        return isSelectSubject;
    }

    public void setSelectSubject(boolean selectSubject) {
        isSelectSubject = selectSubject;
    }

    public String getSubjectID() {
        return SubjectID;
    }

    public void setSubjectID(String subjectID) {
        SubjectID = subjectID;
    }

    public String getSubjectName() {
        return SubjectName;
    }

    public void setSubjectName(String subjectName) {
        SubjectName = subjectName;
    }
}
