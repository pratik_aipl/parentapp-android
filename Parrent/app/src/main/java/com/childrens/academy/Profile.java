package com.childrens.academy;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bluelinelabs.logansquare.LoganSquare;
import com.childrens.academy.base.ActivityBase;
import com.childrens.academy.bean.UserData;
import com.childrens.academy.network.NetworkRequest;
import com.childrens.academy.utils.Constant;
import com.childrens.academy.utils.Utils;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscription;

public class Profile extends ActivityBase {


    @BindView(R.id.mBackBtn)
    ImageView mBackBtn;
    @BindView(R.id.mPageTitle)
    TextView mPageTitle;
    @BindView(R.id.mSearch)
    ImageView mSearch;
    @BindView(R.id.mUserImage)
    ImageView mUserImage;
    @BindView(R.id.mUserName)
    TextView mUserName;
    @BindView(R.id.mStudentRegisterId)
    TextView mStudentRegisterId;

    @BindView(R.id.mEditProfile)
    ImageView mEditProfile;
    @BindView(R.id.mUserName1)
    EditText mUserName1;
    @BindView(R.id.mEmail)
    EditText mEmail;
    @BindView(R.id.mMobileNo)
    EditText mMobileNo;
    @BindView(R.id.mBoard)
    EditText mBoard;
    @BindView(R.id.mMedium)
    EditText mMedium;
    @BindView(R.id.mStandard)
    EditText mStandard;
    @BindView(R.id.mBranch)
    EditText mBranch;
    @BindView(R.id.mBatch)
    EditText mBatch;
    @BindView(R.id.mProgrees)
    ProgressBar mProgrees;

    boolean isEdit = false;

    Subscription subscriptionProfile;
    UserData userData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        ButterKnife.bind(this);

        mPageTitle.setText("Profile");
        editMode(isEdit);
        if (Utils.isNetworkAvailable(this, true))
            getUserData(true);
        mPageTitle.requestFocus();

    }


    private void getUserData(boolean isShow) {
        Map<String, String> map = new HashMap<>();
        showProgress(isShow);
        subscriptionProfile = NetworkRequest.performAsyncRequest(restApi.getProfile(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());
                    userData = LoganSquare.parse(jsonResponse.getJSONObject(Constant.data).toString(), UserData.class);
                    setData(userData);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                Utils.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });
    }

    private void setData(UserData userData) {
        mUserName.setText(userData.getFirstName() + " " + userData.getLastName());
        mUserName1.setText(userData.getFirstName() + " " + userData.getLastName());
        mStudentRegisterId.setText(userData.getUserName());
        mEmail.setText(userData.getEmail());
        mBatch.setText(userData.getBatchName());
        mBranch.setText(userData.getBranchName());
        mStandard.setText(userData.getStandardName());
        mMedium.setText(userData.getMediumName());
        mBoard.setText(userData.getBoardName());
        mMobileNo.setText(userData.getMobileNo());
    }


    @OnClick({R.id.mBackBtn, R.id.mSearch, R.id.mEditProfile})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.mBackBtn:
                onBackPressed();
                break;
            case R.id.mSearch:
                break;
            case R.id.mEditProfile:
                isEdit = !isEdit;
                editMode(isEdit);
                break;
        }
    }

    private void editMode(boolean isEdit) {
        if (isEdit) {
            mEditProfile.setImageResource(R.mipmap.save);
        } else {
            mEditProfile.setImageResource(R.mipmap.edit);
        }
        mUserName1.setEnabled(isEdit);
        mEmail.setEnabled(isEdit);
        mMobileNo.setEnabled(false);
        mBoard.setEnabled(false);
        mMedium.setEnabled(false);
        mStandard.setEnabled(false);
        mBranch.setEnabled(false);
        mBatch.setEnabled(false);
    }
}
