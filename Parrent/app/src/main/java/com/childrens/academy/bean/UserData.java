package com.childrens.academy.bean;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

@JsonObject
public class UserData {

    /*
    "firstname": "Students",
		"lastname": "Bachani",
		"username": "S0002-02",
		"email": "student2@gmail.com",
		"mobileno": "1234567890",
		"BoardName": "CBSE",
		"MediumName": "ENGLISH",
		"StandardName": "VI",
		"BatchName": "Batch A",
		"BranchName": "Asha Nagar Kandivali"
     */
    @JsonField(name = "id")
    String id;

    @JsonField(name = "firstname")
    String firstName;

    @JsonField(name = "lastname")
    String lastName;

    @JsonField(name = "username")
    String userName;

    @JsonField(name = "email")
    String email;

    @JsonField(name = "mobileno")
    String mobileNo;

    @JsonField(name = "BoardName")
    String boardName;

    @JsonField(name = "MediumName")
    String mediumName;

    @JsonField(name = "StandardName")
    String standardName;

    @JsonField(name = "BatchName")
    String batchName;

    @JsonField(name = "BranchName")
    String branchName;

    public String getBoardName() {
        return boardName;
    }

    public void setBoardName(String boardName) {
        this.boardName = boardName;
    }

    public String getMediumName() {
        return mediumName;
    }

    public void setMediumName(String mediumName) {
        this.mediumName = mediumName;
    }

    public String getStandardName() {
        return standardName;
    }

    public void setStandardName(String standardName) {
        this.standardName = standardName;
    }

    public String getBatchName() {
        return batchName;
    }

    public void setBatchName(String batchName) {
        this.batchName = batchName;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }
}

