package com.childrens.academy.bean;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.util.List;

@JsonObject
public class TestSummary {

    @JsonField(name = "test_details")
    TestDetails testDetails;

    @JsonField(name = "corret_questions")
    List<AnswerBean> correctQuestions;

    @JsonField(name = "incorret_questions")
    List<AnswerBean> incorrectQuestions;

    @JsonField(name = "not_appeared_questions")
    List<AnswerBean> notAppearedQuestions;

    public TestDetails getTestDetails() {
        return testDetails;
    }

    public void setTestDetails(TestDetails testDetails) {
        this.testDetails = testDetails;
    }

    public List<AnswerBean> getCorrectQuestions() {
        return correctQuestions;
    }

    public void setCorrectQuestions(List<AnswerBean> correctQuestions) {
        this.correctQuestions = correctQuestions;
    }

    public List<AnswerBean> getIncorrectQuestions() {
        return incorrectQuestions;
    }

    public void setIncorrectQuestions(List<AnswerBean> incorrectQuestions) {
        this.incorrectQuestions = incorrectQuestions;
    }

    public List<AnswerBean> getNotAppearedQuestions() {
        return notAppearedQuestions;
    }

    public void setNotAppearedQuestions(List<AnswerBean> notAppearedQuestions) {
        this.notAppearedQuestions = notAppearedQuestions;
    }
}
