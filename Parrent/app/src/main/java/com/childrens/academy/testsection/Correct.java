package com.childrens.academy.testsection;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.childrens.academy.R;
import com.childrens.academy.adapter.SummaryCorrectAnswerAdapter;
import com.childrens.academy.base.BaseFragment;
import com.childrens.academy.bean.AnswerBean;
import com.childrens.academy.utils.Constant;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class Correct extends BaseFragment {

    @BindView(R.id.mRecyclerView)
    RecyclerView mRecyclerView;
    Unbinder unbinder;

    SummaryCorrectAnswerAdapter summaryCorrectAnswerAdapter;
    List<AnswerBean> answerBeans = new ArrayList<>();
    @BindView(R.id.mEmpty)
    ImageView mEmpty;
    @BindView(R.id.tv_empty)
    TextView tvEmpty;
    @BindView(R.id.mEmptyView)
    RelativeLayout mEmptyView;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public static Correct newInstance(List<AnswerBean> answerBeans) {
        Correct f = new Correct();
        Bundle b = new Bundle();
        b.putSerializable(Constant.ansList, (Serializable) answerBeans);
        f.setArguments(b);
        return f;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.list_view, null);
        unbinder = ButterKnife.bind(this, rootView);
        answerBeans.clear();
        answerBeans = (List<AnswerBean>) getArguments().getSerializable(Constant.ansList);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        summaryCorrectAnswerAdapter = new SummaryCorrectAnswerAdapter(getActivity(), answerBeans, Constant.correct);
        mRecyclerView.setAdapter(summaryCorrectAnswerAdapter);
        mEmpty.setImageDrawable(ContextCompat.getDrawable(getActivity(),R.mipmap.sad));
        tvEmpty.setTextColor(ContextCompat.getColor(getActivity(),R.color.colorPrimary));
        tvEmpty.setText("No any one correct answer.");
        if (answerBeans.size()>0){
            mRecyclerView.setVisibility(View.VISIBLE);
            mEmptyView.setVisibility(View.GONE);
        }else {
            mRecyclerView.setVisibility(View.GONE);
            mEmptyView.setVisibility(View.VISIBLE);
        }
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
