package com.childrens.academy.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.childrens.academy.R;
import com.childrens.academy.bean.OptionBean;
import com.childrens.academy.utils.Utils;
import com.childrens.academy.view.OptionsWebView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Pratik on 04/12/18.
 */
public class OptionAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = "OptionAdapter";
    public final static int VIEW_TYPE_ITEM = 0;
    Context context;
    List<OptionBean> optionBeans;
    private RadioButton mCheckAddressTemp = null;
    private int lastCheckedPos = 0;

    public OptionAdapter(Context context, List<OptionBean> optionBeans) {
        this.context = context;
        this.optionBeans = optionBeans;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.raw_question_option_item, parent, false));


    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holderIn, int position) {
        ViewHolder holder = (ViewHolder) holderIn;
        OptionBean beanData = optionBeans.get(position);
//        holder.mOptionsText.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null);
        holder.mOptionsText.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        holder.mOptionsText.setScrollbarFadingEnabled(false);
        holder.mOptionsText.setInitialScale(40);
        holder.mOptionsText.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null);
        holder.mOptionsText.loadHtmlFromLocal(beanData.getOptions());
        holder.mRadioBtn.setText(Utils.getOptionText(position));

        holder.mViewContainer.bringToFront();
        holder.mRadioBtn.setChecked(beanData.isSelected());

        if (beanData.isSelected()) {

            mCheckAddressTemp = holder.mRadioBtn;
            lastCheckedPos = position;

            if (position == 0)
                holder.mViewContainer.setBackground(ContextCompat.getDrawable(context, R.drawable.list_top_cornor_round_sel));
            else if (position == optionBeans.size() - 1)
                holder.mViewContainer.setBackground(ContextCompat.getDrawable(context, R.drawable.list_bottom_cornor_round_sel));
            else
                holder.mViewContainer.setBackground(ContextCompat.getDrawable(context, R.drawable.list_no_round_sel));
        } else {
            if (position == 0)
                holder.mViewContainer.setBackground(ContextCompat.getDrawable(context, R.drawable.list_top_cornor_round));
            else if (position == optionBeans.size() - 1)
                holder.mViewContainer.setBackground(ContextCompat.getDrawable(context, R.drawable.list_bottom_cornor_round));
            else
                holder.mViewContainer.setBackground(ContextCompat.getDrawable(context, R.drawable.list_no_round));
        }


        holder.mViewContainer.setOnClickListener(v -> {
            if (mCheckAddressTemp != null) {
                optionBeans.get(lastCheckedPos).setSelected(false);
                mCheckAddressTemp.setChecked(false);
            }
            mCheckAddressTemp = holder.mRadioBtn;
            lastCheckedPos = position;
            optionBeans.get(position).setSelected(true);
            optionBeans.get(position).setAttempt(true);
            notifyDataSetChanged();
        });

        holder.mOptionsText.setOnTouchListener((view, event) -> {
            if (view.getId() == R.id.mOptionsText && event.getAction() == MotionEvent.ACTION_DOWN) {
                holder.mViewContainer.performClick();
            }
            return false;
        });
    }

    @Override
    public int getItemViewType(int position) {
        return VIEW_TYPE_ITEM;
    }

    @Override
    public int getItemCount() {

        return optionBeans.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.mViewContainer)
        public LinearLayout mViewContainer;
        @BindView(R.id.mOptionsText)
        public OptionsWebView mOptionsText;
        @BindView(R.id.mRadioBtn)
        public RadioButton mRadioBtn;

        public ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }

}
