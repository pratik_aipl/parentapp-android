package com.childrens.academy.testsection;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bluelinelabs.logansquare.LoganSquare;
import com.childrens.academy.R;
import com.childrens.academy.adapter.QuestionPagerAdaptor;
import com.childrens.academy.base.ActivityBase;
import com.childrens.academy.bean.AnswerDetailBean;
import com.childrens.academy.bean.OptionBean;
import com.childrens.academy.bean.PaperBean;
import com.childrens.academy.bean.QuestionsBean;
import com.childrens.academy.listner.DialogButtonListener;
import com.childrens.academy.network.NetworkRequest;
import com.childrens.academy.utils.Constant;
import com.childrens.academy.utils.Utils;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscription;

public class TodayTest extends ActivityBase {

    private static final String TAG = "TodayTest";
    @BindView(R.id.mBackBtn)
    ImageView mBackBtn;
    @BindView(R.id.mPageTitle)
    TextView mPageTitle;
    @BindView(R.id.mSearch)
    ImageView mSearch;
    @BindView(R.id.mLabel)
    TextView mRightText;
    @BindView(R.id.mPager)
    ViewPager mPager;
    QuestionPagerAdaptor questionPagerAdaptor;
    List<QuestionsBean> questionsBeans = new ArrayList<>();
    @BindView(R.id.mEndTest)
    Button mEndTest;
    @BindView(R.id.mPrev)
    ImageView mPrev;
    @BindView(R.id.mNext)
    ImageView mNext;

    PaperBean paperBean;
    Subscription subscriptionQuestionsList, subscriptionSubmitTest;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.today_test);
        ButterKnife.bind(this);
        paperBean = (PaperBean) getIntent().getSerializableExtra(Constant.selPaper);
        mEndTest.setVisibility(View.VISIBLE);
        mPageTitle.setText(paperBean.getSubjectName());

        if (Utils.isNetworkAvailable(this, true))
            getQuestionsList(true);

        setQuestionPage();

    }


    private void getQuestionsList(boolean isShow) {
        Map<String, String> map = new HashMap<>();
        map.put(Constant.PaperID, paperBean.getMCQPlannerID());
        showProgress(isShow);
        subscriptionQuestionsList = NetworkRequest.performAsyncRequest(restApi.getPaperQuestionsList(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());
                    questionsBeans.clear();
                    questionsBeans.addAll(LoganSquare.parseList(jsonResponse.getJSONArray(Constant.data).toString(), QuestionsBean.class));
                    questionPagerAdaptor.notifyDataSetChanged();
                    updateIndicatorTv();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                Utils.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });
    }

    private void setQuestionPage() {
        questionPagerAdaptor = new QuestionPagerAdaptor(getSupportFragmentManager(), questionsBeans);
        mPager.setAdapter(questionPagerAdaptor);

        mPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                if (position == 0) {
                    mPrev.setBackground(ContextCompat.getDrawable(TodayTest.this, R.drawable.circle));
                } else if (position == (mPager.getAdapter().getCount() - 1)) {
                    mNext.setBackground(ContextCompat.getDrawable(TodayTest.this, R.drawable.circle));
                } else {
                    mPrev.setBackground(ContextCompat.getDrawable(TodayTest.this, R.drawable.circle_primary));
                    mNext.setBackground(ContextCompat.getDrawable(TodayTest.this, R.drawable.circle_primary));
                }
                updateIndicatorTv();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        updateIndicatorTv();


    }

    private void updateIndicatorTv() {
        int totalNum = mPager.getAdapter().getCount();
        int currentItem = mPager.getCurrentItem() + 1;
        mRightText.setText(currentItem + " / " + totalNum);
    }


    @OnClick({R.id.mPrev, R.id.mNext, R.id.mBackBtn, R.id.mEndTest})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.mBackBtn:
                onBackPressed();
                break;
            case R.id.mEndTest:
                Utils.showTwoButtonDialog(this, "End Test", "Do you really want to submit the test?", "Submit", "Cancel", new DialogButtonListener() {
                    @Override
                    public void onPositiveButtonClicked() {
                        if (Utils.isNetworkAvailable(TodayTest.this, true))
                            submitTest(true);
                    }

                    @Override
                    public void onNegativButtonClicked() {
                    }
                });

                break;
            case R.id.mPrev:
                if (mPager.getCurrentItem() > 0) {
                    mPager.setCurrentItem(mPager.getCurrentItem() - 1);
                }
                break;
            case R.id.mNext:
                if (mPager.getCurrentItem() != mPager.getAdapter().getCount()) {
                    mPager.setCurrentItem(mPager.getCurrentItem() + 1);
                }
                break;
        }
    }

    private void submitTest(boolean isShow) {

        List<AnswerDetailBean> answerDetailBeans = new ArrayList<>();
        for (int i = 0; i < questionsBeans.size(); i++) {
            OptionBean selectOption = new OptionBean();
            for (int j = 0; j < questionsBeans.get(i).getOptions().size(); j++) {
                OptionBean optionBean = questionsBeans.get(i).getOptions().get(j);
                selectOption.setMCQOptionID(optionBean.getMCQOptionID());
                if (optionBean.isAttempt()) {
                    selectOption.setAttempt(true);
                    break;
                } else {
                    selectOption.setAttempt(false);
                }
            }
            answerDetailBeans.add(new AnswerDetailBean(questionsBeans.get(i).getMCQPlannerDetailsID(), selectOption.isAttempt() ? selectOption.getMCQOptionID() : 0, selectOption.isAttempt() ? "1" : "0"));
        }

        Log.d(TAG, "submitTest: " + gson.toJson(answerDetailBeans));
        Map<String, String> map = new HashMap<>();
        map.put(Constant.PaperID, paperBean.getMCQPlannerID());
        map.put(Constant.detailArray, gson.toJson(answerDetailBeans));
        showProgress(isShow);
        subscriptionSubmitTest = NetworkRequest.performAsyncRequest(restApi.submitTest(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());
                    JSONObject jsonObject = jsonResponse.getJSONObject(Constant.data);
                    if (jsonObject.has(Constant.StudentMCQTestID)) {
                        startActivity(new Intent(this, TodayTestReport.class)
                                .putExtra(Constant.isTest, true)
                                .putExtra(Constant.StudentMCQTestID, jsonObject.getString(Constant.StudentMCQTestID))
                                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                                        | Intent.FLAG_ACTIVITY_CLEAR_TASK |
                                        Intent.FLAG_ACTIVITY_SINGLE_TOP |
                                        Intent.FLAG_ACTIVITY_CLEAR_TOP));
                        overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
                    }else {

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                Utils.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });

    }

}
