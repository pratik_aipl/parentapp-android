package com.childrens.academy.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;
import android.text.Html;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.childrens.academy.BuildConfig;
import com.childrens.academy.MobileScreen;
import com.childrens.academy.R;
import com.childrens.academy.bean.UserData;
import com.childrens.academy.listner.DialogButtonListener;
import com.google.gson.Gson;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import retrofit2.Response;

public class Utils {
    private static final String TAG = "Utils";
    static boolean isShowing = false;


    public static String getDeviceId(Context context) {
        return Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID);
    }

    public static boolean isLogin(Context context) {
        return Prefs.with(context).getBoolean(Constant.isLogin, false);
    }


    public static String getAuthtoken(Context context) {
        return Prefs.with(context).getString(Constant.login_token, "");
    }

    public static UserData getUser(Context context) {
        return new Gson().fromJson(Prefs.with(context).getString(Constant.UserData, ""), UserData.class);
    }

    public static boolean isNetworkAvailable(Context context, boolean isShow) {

        boolean isConnected = false;
        if (!Constant.isAlertShow) {
            if (!isNetworkConnected(context)) {
                if (isShow) {
                    showTwoButtonDialog(context, context.getString(R.string.network_error_title), context.getString(R.string.network_connection_error), "Ok", null, new DialogButtonListener() {
                        @Override
                        public void onPositiveButtonClicked() {
                            Constant.isAlertShow = false;
                        }

                        @Override
                        public void onNegativButtonClicked() {
                            Constant.isAlertShow = false;
                        }
                    });
                    Constant.isAlertShow = true;
                }
                isConnected = false;
            } else
                isConnected = true;
        }
        return isConnected;
    }

    public static boolean isNetworkConnected(Context c) {
        boolean isConnected = false;
        ConnectivityManager cm = (ConnectivityManager) c.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null) {
            // connected to the internet
            switch (activeNetwork.getType()) {
                case ConnectivityManager.TYPE_WIFI:
                    // connected to wifi
                    isConnected = true;
                    break;
                case ConnectivityManager.TYPE_MOBILE:
                    // connected to mobile data
                    isConnected = true;
                    break;
                default:
                    break;
            }
        } else {
            return false;
        }
        return isConnected;
    }
    private static void generalOkAlert(Context context, String message, Response<String> data) {
        if (context == null)
            return;
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(message).setPositiveButton(context.getResources().getString(R.string.ok), (dialog, which) -> {
            switch (which) {
                case DialogInterface.BUTTON_POSITIVE:
                    dialog.dismiss();
                    isShowing = false;
                    if (data != null && data.code() == 401)
                        logout(context);
                    break;
            }
        });
        if (!isShowing) {
            builder.show();
            isShowing = true;
        }

    }
    public static void logout(Context context) {
        Prefs prefs = Prefs.with(context);
        prefs.save(Constant.isLogin, false);
        Intent intent = new Intent(context, MobileScreen.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        context.startActivity(intent);
    }
    public static void showTwoButtonDialog(Context context, String title, String message, String yesButtonName, String noButtonName, DialogButtonListener dialogButtonListener) {

        android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(context);
        if (title != null)
            alertDialogBuilder.setTitle(title);
        else {
            alertDialogBuilder.setTitle("");
        }
        alertDialogBuilder
                .setMessage(Html.fromHtml(message))
                .setCancelable(false);
        if (yesButtonName != null) {
            yesButtonName = yesButtonName.equals("") ? "YES" : yesButtonName;
            alertDialogBuilder.setPositiveButton(yesButtonName, (dialog, id) -> {
                dialog.cancel();
                if (dialogButtonListener != null) {
                    dialogButtonListener.onPositiveButtonClicked();
                }
            });
        }

        if (noButtonName != null) {
            noButtonName = noButtonName.equals("") ? "NO" : noButtonName;
            alertDialogBuilder.setNegativeButton(noButtonName, (dialog, id) -> {
                dialog.cancel();
                if (dialogButtonListener != null) {
                    dialogButtonListener.onNegativButtonClicked();
                }
            });
        }
        // create alert dialog
        android.support.v7.app.AlertDialog alertDialog = alertDialogBuilder.create();
        // show it
        alertDialog.show();
    }
    public static android.support.v7.app.AlertDialog showOneButtonDialog(Context context, String title, String message) {

        android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(context);
        if (title != null)
            alertDialogBuilder.setTitle(title);
        else {
            alertDialogBuilder.setTitle("");
        }
        alertDialogBuilder
                .setMessage(Html.fromHtml(message));
        alertDialogBuilder.setPositiveButton("OK", null);
        android.support.v7.app.AlertDialog alertDialog = alertDialogBuilder.create();

        return alertDialog;
    }
    public static void serviceStatusFalseProcess(Context context, Response<String> data) {
        try {
           /* if (data.code() == 401) {
                L.logout(context);
            } else */
            if (data.code() == 500) {
                generalOkAlert(context, data.message(), data);
            } else {
                String msg;
                String str = data.errorBody().string();
                JSONObject jsonResponse = new JSONObject(str);
                if (jsonResponse.has(Constant.messageAr) && isArabicLanguage(context)) {
                    msg = jsonResponse.getString(Constant.messageAr);
                } else {
                    msg = jsonResponse.getString(Constant.message);
                }
                generalOkAlert(context, msg, data);
            }

        } catch (Exception e) {
            e.printStackTrace();
            generalOkAlert(context, "oops!\nsomething went wrong.\nPlease try again!", null);
            e.printStackTrace();
        }
    }
    public static boolean isArabicLanguage(Context context) {
        Locale current = context.getResources().getConfiguration().locale;
        String language = current.getLanguage();
        return language.equals("ar");
    }

    public static void loadImageWithPicasso(Context context, String imagePath, ImageView iv, ProgressBar mProgress) {
        if (!imagePath.isEmpty()) {

            if (mProgress != null)
                mProgress.setVisibility(View.VISIBLE);
            Picasso.with(context)
                    .load(imagePath)
                    .error(R.mipmap.zooki_image).into(iv, new Callback() {
                @Override
                public void onSuccess() {
                    if (mProgress != null)
                        mProgress.setVisibility(View.GONE);
                }

                @Override
                public void onError() {
                    if (mProgress != null)
                        mProgress.setVisibility(View.GONE);
                }
            });

        } else
            Picasso.with(context).load(R.mipmap.zooki_image).into(iv);
    }
    public static CharSequence printDifference(String start) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sdf.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
        long time = 0;

        try {
            time = sdf.parse(start).getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long now = System.currentTimeMillis();

        CharSequence ago =
                DateUtils.getRelativeTimeSpanString(time, now, DateUtils.MINUTE_IN_MILLIS);
        return ago;
    }
    public static String changeDateToDDMMYYYY(String input) {
        String outputDateStr;
        try {
            DateFormat outputFormat = new SimpleDateFormat("dd-MM-yyyy");
            DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = inputFormat.parse(input);
            outputDateStr = outputFormat.format(date);
            return outputDateStr;
        } catch (ParseException p) {
            p.printStackTrace();
        }
        return "";
    }

    public static String changeDateToDDMMMYYYY(String input) {
        String outputDateStr;
        try {
            DateFormat outputFormat = new SimpleDateFormat("dd-MMM-yyyy");
            DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = inputFormat.parse(input);
            outputDateStr = outputFormat.format(date);
            return outputDateStr;
        } catch (ParseException p) {
            p.printStackTrace();
        }
        return "";
    }

    public static String changeDateToDay(String input) {
        String outputDateStr;
        try {
            DateFormat outputFormat = new SimpleDateFormat("dd");
            DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = inputFormat.parse(input);
            outputDateStr = outputFormat.format(date);
            return outputDateStr;
        } catch (ParseException p) {
            p.printStackTrace();
        }
        return "";
    }
    public static String changeDateToMonth(String input) {
        String outputDateStr;
        try {
            DateFormat outputFormat = new SimpleDateFormat("MMM");
            DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = inputFormat.parse(input);
            outputDateStr = outputFormat.format(date);
            return outputDateStr;
        } catch (ParseException p) {
            p.printStackTrace();
        }
        return "";
    }
    public static String getOptionText(int position) {
        return String.valueOf((char)(65+position));
    }
    public static String getDurationBreakdown(long diff) {
        long millis = diff;
        if (millis < 0) {
            return "00:00:00";
        }
        long hours = TimeUnit.MILLISECONDS.toHours(millis);
        millis -= TimeUnit.HOURS.toMillis(hours);
        long minutes = TimeUnit.MILLISECONDS.toMinutes(millis);
        millis -= TimeUnit.MINUTES.toMillis(minutes);
        long seconds = TimeUnit.MILLISECONDS.toSeconds(millis);

        Log.d(TAG, "getDurationBreakdown: "+millis);
        Log.d(TAG, "getDurationBreakdown: "+hours);
        Log.d(TAG, "getDurationBreakdown: "+minutes);
        Log.d(TAG, "getDurationBreakdown: "+seconds);
        return String.format(Locale.ENGLISH, "%02d:%02d:%02d", hours, minutes, seconds);
        //return "${getWithLeadZero(hours)}:${getWithLeadZero(minutes)}:${getWithLeadZero(seconds)}"
    }
    public static long strToMilli(String strTime) {
        long retVal = 0;
        String hour = strTime.substring(0, 2);
        String min = strTime.substring(3, 5);
        int h = Integer.parseInt(hour);
        int m = Integer.parseInt(min);
        int s = Integer.parseInt("00");

        String strDebug = String.format("%02d:%02d:%02d", h, m, s);
        if (BuildConfig.DEBUG)
            Log.d(TAG, "strToMilli: " + strDebug);
        long lH = h * 60 * 60 * 1000;
        long lM = m * 60 * 1000;
        long lS = s * 1000;

        retVal = lH + lM + lS;
        return retVal;
    }
    public static String formatTime(long millis) {
        String output = "00:00:00";
        long seconds = millis / 1000;
        long minutes = seconds / 60;
        long hours = minutes / 60;

        seconds = seconds % 60;
        minutes = minutes % 60;
        hours = hours % 60;

        String secondsD = String.valueOf(seconds);
        String minutesD = String.valueOf(minutes);
        String hoursD = String.valueOf(hours);

        if (seconds < 10)
            secondsD = "0" + seconds;
        if (minutes < 10)
            minutesD = "0" + minutes;
        if (hours < 10)
            hoursD = "0" + hours;

        output = hoursD + ":" + minutesD /*+ " : " + secondsD*/;
        return output;
    }
}
