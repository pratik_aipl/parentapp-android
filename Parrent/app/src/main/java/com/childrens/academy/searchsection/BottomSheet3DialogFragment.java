package com.childrens.academy.searchsection;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.childrens.academy.R;
import com.childrens.academy.adapter.LevelAdapter;
import com.childrens.academy.adapter.SubjectAdapter;
import com.childrens.academy.bean.LevelBean;
import com.childrens.academy.bean.SubjectBean;
import com.childrens.academy.listner.LevelOnClick;
import com.childrens.academy.listner.PageReloadEvent;
import com.childrens.academy.listner.SubjectOnClick;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class BottomSheet3DialogFragment extends BottomSheetDialogFragment implements SubjectOnClick, LevelOnClick {

    @BindView(R.id.rSubject)
    RadioButton rSubject;
    @BindView(R.id.rLevel)
    RadioButton rLevel;
    @BindView(R.id.rgFilterType)
    RadioGroup rgFilterType;
    @BindView(R.id.mSubjectList)
    RecyclerView mSubjectList;
    @BindView(R.id.mLevelList)
    RecyclerView mLevelList;
    @BindView(R.id.mReset)
    Button mReset;
    @BindView(R.id.mSave)
    Button mSave;
    Unbinder unbinder;
    List<String> levelIDS = new ArrayList<>();
    List<String> subjectIDS = new ArrayList<>();
    SubjectAdapter subjectAdapter;
    LevelAdapter levelAdapter;
    static List<LevelBean> levelBeanList1 = new ArrayList<>();
    static List<SubjectBean> subjectBeanList1 = new ArrayList<>();


    public static BottomSheet3DialogFragment newInstance(List<LevelBean> levelBeanList, List<SubjectBean> subjectBeanList) {
        levelBeanList1 = levelBeanList;
        subjectBeanList1 = subjectBeanList;
        return new BottomSheet3DialogFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fillter_bottom_dailog, container, false);
        unbinder = ButterKnife.bind(this, view);

        mSubjectList.setLayoutManager(new LinearLayoutManager(getActivity()));
        subjectAdapter = new SubjectAdapter(getActivity(),subjectBeanList1,BottomSheet3DialogFragment.this);
        mSubjectList.setAdapter(subjectAdapter);

        mLevelList.setLayoutManager(new LinearLayoutManager(getActivity()));
        levelAdapter = new LevelAdapter(getActivity(),levelBeanList1,BottomSheet3DialogFragment.this);
        mLevelList.setAdapter(levelAdapter);

        rgFilterType.setOnCheckedChangeListener((group, checkedId) -> {

            switch (checkedId) {
                case R.id.rSubject:
                    mSubjectList.setVisibility(View.VISIBLE);
                    mLevelList.setVisibility(View.GONE);
                    break;
                case R.id.rLevel:
                    mLevelList.setVisibility(View.VISIBLE);
                    mSubjectList.setVisibility(View.GONE);
                    break;
            }

        });

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.mReset, R.id.mSave})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.mReset:
                levelIDS.clear();
                subjectIDS.clear();
                break;
            case R.id.mSave:
                ((SearchTestPaper)getActivity()).bottomSheet3DialogFragment.dismiss();
                EventBus.getDefault().post(new PageReloadEvent(true,android.text.TextUtils.join(",", subjectIDS),android.text.TextUtils.join(",", levelIDS)));
                break;
        }
    }

    @Override
    public void callBackLevel(int pos, LevelBean subjectBean) {
        levelIDS.clear();
        for (int i = 0; i < levelBeanList1.size(); i++) {
            if (levelBeanList1.get(i).isSelectLevel()) {
                levelIDS.add(levelBeanList1.get(i).getLevelID());
            }
        }
    }

    @Override
    public void callBackSubject(int pos, SubjectBean subjectBean) {
        subjectIDS.clear();
        for (int i = 0; i < subjectBeanList1.size(); i++) {
            if (subjectBeanList1.get(i).isSelectSubject()) {
                subjectIDS.add(subjectBeanList1.get(i).getSubjectID());
            }
        }

    }
}
