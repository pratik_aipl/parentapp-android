package com.childrens.academy.zookisection;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.childrens.academy.R;
import com.childrens.academy.base.BaseFragment;
import com.childrens.academy.bean.ZookiBean;
import com.childrens.academy.observscroll.ObservableScrollView;
import com.childrens.academy.observscroll.ObservableScrollViewCallbacks;
import com.childrens.academy.observscroll.ScrollState;
import com.childrens.academy.utils.Constant;
import com.childrens.academy.utils.Utils;
import com.nineoldandroids.view.ViewHelper;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


public class ZookiFragment extends BaseFragment implements ObservableScrollViewCallbacks {

    public ZookiBean zookiObj;
    public int mParallaxImageHeight;
    @BindView(R.id.mTvHeadTitle)
    TextView mTvHeadTitle;
    @BindView(R.id.mZookiImage)
    ImageView mZookiImage;
    @BindView(R.id.anchor)
    View anchor;
    @BindView(R.id.mTvTitle)
    TextView mTvTitle;
    @BindView(R.id.mTvDate)
    TextView mTvDate;
    @BindView(R.id.mTvFullDesc)
    TextView mTvFullDesc;
    @BindView(R.id.mScrollView)
    ObservableScrollView mScrollView;
    @BindView(R.id.mFrameContent)
    FrameLayout mFrameContent;
    @BindView(R.id.mBtnViewMore)
    Button mBtnViewMore;
    @BindView(R.id.mFrameViewMore)
    FrameLayout mFrameViewMore;
    Unbinder unbinder;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_common, null);
        unbinder = ButterKnife.bind(this, rootView);

        if (zookiObj != null && zookiObj.getTitle().equals("View More")) {
            mFrameContent.setVisibility(View.GONE);
            mFrameViewMore.setVisibility(View.VISIBLE);
            mFrameViewMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    if (Utils.isNetworkAvailable(getActivity())) {
//                        startActivity(new Intent(getActivity(), ZookiWebActivity.class));
//                    } else {
//                        Utils.showToast(getResources().getString(R.string.conect_internet), getActivity());
//                    }
                }
            });
        } else {
            mTvTitle.setText(zookiObj.getTitle());
            mTvHeadTitle.setText(zookiObj.getTitle());
            String date = zookiObj.getCreatedDate();
            mTvDate.setText(Utils.changeDateToDDMMYYYY(date));
            mTvFullDesc.setText(zookiObj.getDescription());
            Utils.loadImageWithPicasso(getActivity(), prefs.getString(Constant.folderPath, "") + zookiObj.getImage(), mZookiImage, null);
            mScrollView.setScrollViewCallbacks(this);
            mParallaxImageHeight = getResources().getDimensionPixelSize(R.dimen._180sdp);
            onScrollChanged(mScrollView.getCurrentScrollY(), false, false);
        }


        return rootView;
    }

    /**
     * @param obj
     */
    public void bindData(ZookiBean obj) {
        this.zookiObj = obj;
    }

    /**
     * @param scrollY     Scroll position in Y axis.
     * @param firstScroll True when this is called for the first time in the consecutive motion events.
     * @param dragging    True when the view is dragged and false when the view is scrolled in the inertia.
     */
    @Override
    public void onScrollChanged(int scrollY, boolean firstScroll, boolean dragging) {
        ViewHelper.setTranslationY(mZookiImage, scrollY / 2);
    }

    @Override
    public void onDownMotionEvent() {

    }

    @Override
    public void onUpOrCancelMotionEvent(ScrollState scrollState) {

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
