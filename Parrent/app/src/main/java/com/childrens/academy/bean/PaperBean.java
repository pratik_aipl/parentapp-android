package com.childrens.academy.bean;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;

@JsonObject
public class PaperBean implements Serializable {


    @JsonField(name = "StudentMCQTestID")
    String StudentMCQTestID;


    @JsonField(name = "MCQPlannerID")
    String MCQPlannerID;

    @JsonField(name = "TotalQuestion")
    int TotalQuestion;

    @JsonField(name = "SubjectName")
    String SubjectName;

    @JsonField(name = "StandardName")
    String StandardName;

    @JsonField(name = "MediumName")
    String MediumName;

    @JsonField(name = "BoardName")
    String BoardName;

    @JsonField(name = "BranchName")
    String BranchName;

    @JsonField(name = "BranchID")
    int BranchID;

    @JsonField(name = "StartDate")
    String StartDate;

    @JsonField(name = "EndDate")
    String EndDate;

    @JsonField(name = "PublishDate")
    String PublishDate;

    @JsonField(name = "Time")
    String Time;

    @JsonField(name = "SubmitDate")
    String SubmitDate;
    @JsonField(name = "TotalRight")
    int TotalRight;
    @JsonField(name = "Accuracy")
    int Accuracy;

    public String getStudentMCQTestID() {
        return StudentMCQTestID;
    }

    public void setStudentMCQTestID(String studentMCQTestID) {
        StudentMCQTestID = studentMCQTestID;
    }

    public int getAccuracy() {
        return Accuracy;
    }

    public void setAccuracy(int accuracy) {
        Accuracy = accuracy;
    }

    public int getTotalRight() {
        return TotalRight;
    }

    public void setTotalRight(int totalRight) {
        TotalRight = totalRight;
    }

    public String getSubmitDate() {
        return SubmitDate;
    }

    public void setSubmitDate(String submitDate) {
        SubmitDate = submitDate;
    }

    public String getPublishDate() {
        return PublishDate;
    }

    public void setPublishDate(String publishDate) {
        PublishDate = publishDate;
    }

    public int getBranchID() {
        return BranchID;
    }


    public void setBranchID(int branchID) {
        BranchID = branchID;
    }

    public String getMCQPlannerID() {
        return MCQPlannerID;
    }

    public void setMCQPlannerID(String MCQPlannerID) {
        this.MCQPlannerID = MCQPlannerID;
    }

    public int getTotalQuestion() {
        return TotalQuestion;
    }

    public void setTotalQuestion(int totalQuestion) {
        TotalQuestion = totalQuestion;
    }

    public String getSubjectName() {
        return SubjectName;
    }

    public void setSubjectName(String subjectName) {
        SubjectName = subjectName;
    }

    public String getStandardName() {
        return StandardName;
    }

    public void setStandardName(String standardName) {
        StandardName = standardName;
    }

    public String getMediumName() {
        return MediumName;
    }

    public void setMediumName(String mediumName) {
        MediumName = mediumName;
    }

    public String getBoardName() {
        return BoardName;
    }

    public void setBoardName(String boardName) {
        BoardName = boardName;
    }

    public String getBranchName() {
        return BranchName;
    }

    public void setBranchName(String branchName) {
        BranchName = branchName;
    }

    public String getStartDate() {
        return StartDate;
    }

    public void setStartDate(String startDate) {
        StartDate = startDate;
    }

    public String getEndDate() {
        return EndDate;
    }

    public void setEndDate(String endDate) {
        EndDate = endDate;
    }

    public String getTime() {
        return Time;
    }

    public void setTime(String time) {
        Time = time;
    }
}
