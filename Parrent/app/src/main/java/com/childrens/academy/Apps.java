package com.childrens.academy;

import android.app.Application;
import android.content.Context;

import com.childrens.academy.notificationreciver.NotificationOpenedHandler;
import com.childrens.academy.notificationreciver.NotificationReceivedHandler;
import com.childrens.academy.utils.Prefs;
import com.onesignal.OneSignal;

import java.util.Locale;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

public class Apps extends Application {


    public static Context mContext;
    private Locale locale = null;

    private Prefs prefs;

    @Override
    public void onCreate() {
        super.onCreate();

        CalligraphyConfig.initDefault(
                new CalligraphyConfig.Builder()
                        .setDefaultFontPath("Sylfaen.ttf")
                        .setFontAttrId(R.attr.fontPath)
                        .build());


        mContext = this;
        prefs = Prefs.with(this);

        // OneSignal Initialization
        OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .setNotificationReceivedHandler(new NotificationReceivedHandler())
                .setNotificationOpenedHandler(new NotificationOpenedHandler(getApplicationContext()))
                .unsubscribeWhenNotificationsAreDisabled(true)
                .init();
    }
}
