package com.childrens.academy;

import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bluelinelabs.logansquare.LoganSquare;
import com.childrens.academy.adapter.LegalAdapter;
import com.childrens.academy.base.ActivityBase;
import com.childrens.academy.bean.LegalData;
import com.childrens.academy.network.NetworkRequest;
import com.childrens.academy.utils.Constant;
import com.childrens.academy.utils.Utils;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscription;

public class Legal extends ActivityBase {

    @BindView(R.id.mBackBtn)
    ImageView mBackBtn;
    @BindView(R.id.mPageTitle)
    TextView mPageTitle;
    @BindView(R.id.mSearch)
    ImageView mSearch;


    Subscription subscriptionLegal;
    @BindView(R.id.mRecyclerView)
    RecyclerView mLegalList;


    private RecyclerView.LayoutManager layoutManager;
    private LegalAdapter legalAdapter;
    List<LegalData> legalList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_legal);
        ButterKnife.bind(this);

        mPageTitle.setText("Legal");


        layoutManager = new LinearLayoutManager(this);
        mLegalList.setLayoutManager(layoutManager);
        mLegalList.setItemAnimator(new DefaultItemAnimator());

        legalAdapter = new LegalAdapter(this, legalList);
        mLegalList.setAdapter(legalAdapter);
        if (Utils.isNetworkAvailable(this, true))
            getLegalData(true);
    }

    @OnClick({R.id.mBackBtn, R.id.mSearch})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.mBackBtn:
                onBackPressed();
                break;
            case R.id.mSearch:
                break;
        }
    }

    @Override
    protected void onDestroy() {
        if (subscriptionLegal != null && !subscriptionLegal.isUnsubscribed()) {
            subscriptionLegal.isUnsubscribed();
            subscriptionLegal = null;
        }
        super.onDestroy();
    }

    private void getLegalData(boolean isShow) {
        Map<String, String> map = new HashMap<>();
        showProgress(isShow);
        subscriptionLegal = NetworkRequest.performAsyncRequest(restApi.getLegal(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());
                    legalList.addAll(LoganSquare.parseList(jsonResponse.getJSONArray(Constant.data).toString(), LegalData.class));
                    legalAdapter.notifyDataSetChanged();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                Utils.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });
    }

}
