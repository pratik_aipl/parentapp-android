package com.childrens.academy.listner;

import com.childrens.academy.bean.PaperBean;

public interface ReportDetailsSubjectOnClick {
    public void callbackPaper(int pos, PaperBean searchBean);
}
