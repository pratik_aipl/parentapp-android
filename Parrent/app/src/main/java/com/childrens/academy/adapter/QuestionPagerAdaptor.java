package com.childrens.academy.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.childrens.academy.bean.QuestionsBean;
import com.childrens.academy.testsection.QuestionPageFragment;

import java.util.List;

public class QuestionPagerAdaptor extends FragmentPagerAdapter {
    List<QuestionsBean> questionsBeans;

    public QuestionPagerAdaptor(FragmentManager fm, List<QuestionsBean> questionsBeans) {
        super(fm);
        this.questionsBeans = questionsBeans;
    }

    @Override
    public Fragment getItem(int i) {
          return QuestionPageFragment.newInstance(i, questionsBeans.get(i));
    }

    @Override
    public int getCount() {
        return questionsBeans.size();
    }
}
