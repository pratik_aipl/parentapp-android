package com.childrens.academy.utils;

import android.os.Environment;

public class Constant {

    public static String LOCAL_IMAGE_PATH = Environment.getExternalStorageDirectory() + "/.iStudent";
    public static String LOCAL_Library_PATH = Environment.getExternalStorageDirectory() + "/Download";


    public static String UserData = "UserData";
    public static String login_token = "login_token";
    public static String isLogin="isLogin";
    public static boolean isAlertShow=false;
    public static String message="message";
    public static String messageAr="messageArabic";
    public static String data="data";
    public static String user_id="user_id";
    public static String mobileno="mobileno";
    public static String otp="otp";
    public static String student="student";
    public static String selPaper="selPaper";
    public static String selQuestion="selQuestion";
    public static String ansList="ansLis";

    public static String correct="Correct";
    public static String notappeared="Not Attempted";
    public static String incorrect="In Correct";
    public static String folderPath="folder_path";
    public static  String StudentMCQTestID = "StudentMCQTestID";

    public static final String MobileNo="MobileNo";
    public static final String UserID = "UserID";
    public static final String OTP = "OTP";
    public static final String PlayerID = "PlayerID";
    public static final String DeviceID = "DeviceID";
    public static final String Message = "Message";
    public static final String PaperID = "PaperID";
    public static final String SubjectID="SubjectID";
    public static final String LevelID =  "LevelID";
    public static final String StudentCode = "StudentCode";
    public static final String AvgTime = "AvgTime";
    public static final String TakenTime = "TakenTime";
    public static final String detailArray="detail_array";

    public static String isTest="isTest";
    public static String type="type";
    public static String parent="parent";
    public static String subjectReport="subjectReport";
}
