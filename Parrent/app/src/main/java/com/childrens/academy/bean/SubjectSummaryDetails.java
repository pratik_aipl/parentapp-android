package com.childrens.academy.bean;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;
import java.util.List;

@JsonObject
public class SubjectSummaryDetails implements Serializable {

    @JsonField
    int OverallPerformance;

    @JsonField
    int TotalAttempt;

    @JsonField
    int TotalAssign;

    @JsonField
    int TotalMissed;

    @JsonField
    List<PaperBean> Paper_info;

    @JsonField
    List<PaperBean> Missed_Paper_info;

    public int getOverallPerformance() {
        return OverallPerformance;
    }

    public void setOverallPerformance(int overallPerformance) {
        OverallPerformance = overallPerformance;
    }

    public int getTotalAttempt() {
        return TotalAttempt;
    }

    public void setTotalAttempt(int totalAttempt) {
        TotalAttempt = totalAttempt;
    }

    public int getTotalAssign() {
        return TotalAssign;
    }

    public void setTotalAssign(int totalAssign) {
        TotalAssign = totalAssign;
    }

    public int getTotalMissed() {
        return TotalMissed;
    }

    public void setTotalMissed(int totalMissed) {
        TotalMissed = totalMissed;
    }

    public List<PaperBean> getPaper_info() {
        return Paper_info;
    }

    public void setPaper_info(List<PaperBean> paper_info) {
        Paper_info = paper_info;
    }

    public List<PaperBean> getMissed_Paper_info() {
        return Missed_Paper_info;
    }

    public void setMissed_Paper_info(List<PaperBean> missed_Paper_info) {
        Missed_Paper_info = missed_Paper_info;
    }
}
