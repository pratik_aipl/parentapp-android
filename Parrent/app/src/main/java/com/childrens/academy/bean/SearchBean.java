package com.childrens.academy.bean;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;

@JsonObject
public class SearchBean implements Serializable {

    @JsonField(name = "StudentMCQTestID")
    String StudentMCQTestID;

    @JsonField(name = "MCQPlannerID")
    int MCQPlannerID;

    @JsonField(name = "TotalQuestion")
    int TotalQuestion;

    @JsonField(name = "SubjectName")
    String SubjectName;

    @JsonField(name = "TotalRight")
    int TotalRight;

    @JsonField(name = "TotalWrong")
    int TotalWrong;

    @JsonField(name = "TotalAttempt")
    int TotalAttempt;

    @JsonField(name = "Accuracy")
    int Accuracy;

    @JsonField(name = "SubmitDate")
    String SubmitDate;

    public String getStudentMCQTestID() {
        return StudentMCQTestID;
    }

    public void setStudentMCQTestID(String studentMCQTestID) {
        StudentMCQTestID = studentMCQTestID;
    }

    public int getMCQPlannerID() {
        return MCQPlannerID;
    }

    public void setMCQPlannerID(int MCQPlannerID) {
        this.MCQPlannerID = MCQPlannerID;
    }

    public int getTotalQuestion() {
        return TotalQuestion;
    }

    public void setTotalQuestion(int totalQuestion) {
        TotalQuestion = totalQuestion;
    }

    public String getSubjectName() {
        return SubjectName;
    }

    public void setSubjectName(String subjectName) {
        SubjectName = subjectName;
    }

    public int getTotalRight() {
        return TotalRight;
    }

    public void setTotalRight(int totalRight) {
        TotalRight = totalRight;
    }

    public int getTotalWrong() {
        return TotalWrong;
    }

    public void setTotalWrong(int totalWrong) {
        TotalWrong = totalWrong;
    }

    public int getTotalAttempt() {
        return TotalAttempt;
    }

    public void setTotalAttempt(int totalAttempt) {
        TotalAttempt = totalAttempt;
    }

    public int getAccuracy() {
        return Accuracy;
    }

    public void setAccuracy(int accuracy) {
        Accuracy = accuracy;
    }

    public String getSubmitDate() {
        return SubmitDate;
    }

    public void setSubmitDate(String submitDate) {
        SubmitDate = submitDate;
    }
}
