package com.childrens.academy.testsection;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bluelinelabs.logansquare.LoganSquare;
import com.childrens.academy.R;
import com.childrens.academy.adapter.CCTAdapter;
import com.childrens.academy.base.ActivityBase;
import com.childrens.academy.bean.PaperBean;
import com.childrens.academy.listner.TestOnClick;
import com.childrens.academy.network.NetworkRequest;
import com.childrens.academy.searchsection.SearchTestPaper;
import com.childrens.academy.utils.Constant;
import com.childrens.academy.utils.Utils;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscription;

public class CCT extends ActivityBase implements TestOnClick {


    @BindView(R.id.mBackBtn)
    ImageView mBackBtn;
    @BindView(R.id.mPageTitle)
    TextView mPageTitle;
    @BindView(R.id.mSearch)
    ImageView mSearch;
    @BindView(R.id.mRecyclerView)
    RecyclerView mRecyclerView;

    Subscription subscriptionPaperList;
    CCTAdapter cctAdapter;
    List<PaperBean> paperBeans = new ArrayList<>();
    @BindView(R.id.mEmpty)
    ImageView mEmpty;
    @BindView(R.id.tv_empty)
    TextView tvEmpty;
    @BindView(R.id.mEmptyView)
    RelativeLayout mEmptyView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_paper);
        ButterKnife.bind(this);

        mPageTitle.setText("ASSIGNMENT");
        mSearch.setVisibility(View.VISIBLE);
        mSearch.setImageResource(R.mipmap.filter);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        cctAdapter = new CCTAdapter(this, paperBeans);
        mRecyclerView.setAdapter(cctAdapter);

        if (Utils.isNetworkAvailable(this, true))
            getPapers(true);
    }


    private void getPapers(boolean isShow) {
        Map<String, String> map = new HashMap<>();
        showProgress(isShow);
        subscriptionPaperList = NetworkRequest.performAsyncRequest(restApi.getPaperList(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());
                    paperBeans.clear();
                    paperBeans.addAll(LoganSquare.parseList(jsonResponse.getJSONArray(Constant.data).toString(), PaperBean.class));
                    cctAdapter.notifyDataSetChanged();
                    mEmpty.setImageDrawable(ContextCompat.getDrawable(this, R.mipmap.happy));
                    tvEmpty.setText("No Assignment Pending");
                    if (paperBeans.size() > 0) {
                        mRecyclerView.setVisibility(View.VISIBLE);
                        mEmptyView.setVisibility(View.GONE);
                    } else {
                        mRecyclerView.setVisibility(View.GONE);
                        mEmptyView.setVisibility(View.VISIBLE);
                    }

//                    initMarkData();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                Utils.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });
    }


    @OnClick({R.id.mBackBtn, R.id.mSearch})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.mBackBtn:
                onBackPressed();
                break;
            case R.id.mSearch:
                startActivity(new Intent(this, SearchTestPaper.class));
                break;
        }
    }


    @Override
    public void callbackPeep(int pos, PaperBean paperBean) {
//        if (paperBean.getTotalQuestion() > 0) {
//            rxPermissions.request(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
//                    .subscribe(granted -> {
//                        if (granted) {
//                            startActivity(new Intent(this, TodayTest.class).putExtra(Constant.selPaper, paperBean));
//
//                        } else {
//                            // Oops permission denied
//                        }
//                    });
//
//        } else {
//
//        }
    }
}
